{-# LANGUAGE NoImplicitPrelude #-}

module Main(
  main
) where

import Data.TheGapChessClub.Schedule(writecalfile, events)
import System.Environment(getArgs)
import System.IO(IO, hPutStrLn, stderr)

main ::
  IO ()
main =
  do  a <- getArgs
      case a of
        [] ->
          hPutStrLn stderr "pass .cal file as argument"
        a':_ ->
          writecalfile a' events

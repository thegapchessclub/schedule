{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Data.TheGapChessClub.Schedule.TgccEvent(
  TgccEvent(..)
, tgccEvent
, confirmedTgccEvent
, tentativeTgccEvent
) where

import Control.Category((.))
import Data.CaseInsensitive(CI)
import Data.Default(def)
import Data.Either(Either(Right, Left))
import Data.Eq(Eq)
import Data.Foldable(toList)
import Data.Functor(fmap)
import Data.Hashable(Hashable(hash, hashWithSalt))
import Data.Int(Int)
import Data.List((++))
import Data.Maybe(Maybe(Nothing, Just))
import Data.Ord(Ord)
import Data.String(String)
import Data.Text.Lazy(Text)
import qualified Data.Text.Lazy as Text(pack)
import Data.Time(UTCTime(UTCTime), addUTCTime, fromGregorian)
import qualified Data.Set as Set(empty, fromList, map)
import Network.URI(URI(URI), URIAuth(URIAuth))
import Prelude(Integer, Show, (*), (+), show)
import Text.ICalendar(OtherParams(OtherParams), OtherParam(OtherParam), VEvent(VEvent), DTStamp(DTStamp), UID(UID), Class(Class), ClassValue(Public), DTStart(DTStartDateTime), DateTime(UTCDateTime), Created(Created), Description(Description), Language(Language), Geo(Geo), LastModified(LastModified), Location(Location), Priority(Priority), Sequence(Sequence), EventStatus(ConfirmedEvent, TentativeEvent, CancelledEvent), Summary(Summary), TimeTransparency(Opaque), DTEnd(DTEndDateTime), Categories(Categories), Contact(Contact))

hashableTgccEvent ::
  TgccEvent
  -> (Text, String, Integer, Int, Int, [String], (Either () (Either () ()), [(CI Text, [Text])]))
hashableTgccEvent (TgccEvent uid description start_year start_month start_date categories status) =
  let otherparams' (OtherParams z) =
        toList (Set.map (\(OtherParam a q) -> (a, q)) z)
      status' =
        case status of
          ConfirmedEvent x ->
            (Left (), otherparams' x)
          TentativeEvent x ->
            (Right (Left ()), otherparams' x)
          CancelledEvent x ->
            (Right (Right ()), otherparams' x)
  in  (uid, description, start_year, start_month, start_date, categories, status')

instance Hashable TgccEvent where
  hash =
    hash . hashableTgccEvent
  hashWithSalt n =
    hashWithSalt n . hashableTgccEvent

data TgccEvent =
  TgccEvent
    Text
    String
    Integer
    Int
    Int
    [String]
    EventStatus
  deriving (Eq, Ord, Show)

confirmedTgccEvent ::
  Text
  -> String
  -> Integer
  -> Int
  -> Int
  -> [String]
  -> TgccEvent
confirmedTgccEvent uid description start_year start_month start_date categories =
  TgccEvent
    uid
    description
    start_year
    start_month
    start_date
    categories
    (ConfirmedEvent def)

tentativeTgccEvent ::
  Text
  -> String
  -> Integer
  -> Int
  -> Int
  -> [String]
  -> TgccEvent
tentativeTgccEvent uid description start_year start_month start_date categories =
  TgccEvent
    uid
    description
    start_year
    start_month
    start_date
    categories
    (TentativeEvent def)

tgccEvent ::
  TgccEvent
  -> UTCTime
  -> ((Text, Maybe a), VEvent)
tgccEvent e@(TgccEvent uid description start_year start_month start_date categories status) now =
  (
    (
      uid
    , Nothing
    )
  , let start_utc_time = 
          addUTCTime
            (-10 * 60 * 60)
            (
              UTCTime
                (
                  fromGregorian
                    start_year
                    start_month
                    start_date
                )
                (19 * 60 * 60 + 30 * 60)
            )
    in  VEvent
          (
            DTStamp
              now
              def
          )
          (
            let h =
                  hashWithSalt 64 e
            in  UID
                  (Text.pack (show h ++ "-schedule@thegapchessclub.org.au"))
                  def
          )
          (
            Class
              Public
              def
          )
          (
            Just
              (
                DTStartDateTime
                  (
                    UTCDateTime
                      start_utc_time
                  )
                  def
              )
          )
          (
            Just
              (
                Created
                  now
                  def
              )
          )
          (
            Just
              (
                Description
                  (Text.pack description)
                  Nothing
                  (
                    Just
                      (
                        Language
                          "English"
                      )
                  )
                  def
              )
          )
          (
            Just
              (
                Geo
                  (-27.445500)
                  152.949750
                  def
              )
          )
          (
            Just
              (
                LastModified
                  now
                  def
              )
          )
          (
            Just
              (
                Location
                  "The Gap State High School, 1020 Waterworks Rd, The Gap QLD 4061, Australia"
                  (
                    Just
                      (
                        URI
                          "https"
                          (
                            Just
                              (
                                URIAuth
                                  ""
                                  "thegapchessclub.org.au"
                                  ""
                              )
                          )
                          "/location"
                          ""
                          ""
                      )
                  )
                  (
                    Just
                      (
                        Language
                          "English"
                      )
                  )
                  def
              )
          )
          Nothing
          (
            Priority
              1
              def
          )
          (
            Sequence
              0
              def
          )
          (
            Just
              status
          )
          (
            Just
              (
                Summary
                  uid
                  Nothing
                  (
                    Just
                      (
                        Language
                          "English"
                      )
                  )
                  def
              )
          )
          (
            Opaque
              def
          )
          Nothing
          Nothing
          Set.empty
          (
            Just
              (
                Left
                  (
                    DTEndDateTime
                      (
                        UTCDateTime
                        (
                          addUTCTime (3 * 60 * 60 + 30 * 60) start_utc_time
                        )
                      )
                      def
                  )
              )
          )
          Set.empty
          Set.empty
          (
            Set.fromList
              [
                Categories
                  (
                    Set.fromList
                      (fmap Text.pack categories)
                  )
                  (
                    Just
                      (
                        Language
                          "English"
                      )
                  )
                  def
              ]
          )
          Set.empty
          (
            Set.fromList
              [
                Contact
                  "contact@thegapchessclub.org.au"
                  (
                    Just
                      (
                        URI
                          "https"
                          (
                            Just
                              (
                                URIAuth
                                  ""
                                  "thegapchessclub.org.au"
                                  ""
                              )
                          )
                          "/contact"
                          ""
                          ""
                      )
                  )
                  (
                    Just
                      (
                        Language
                          "English"
                      )
                  )
                  def
              ]
          )
          Set.empty
          Set.empty
          Set.empty
          Set.empty
          Set.empty
          Set.empty
          Set.empty
      )

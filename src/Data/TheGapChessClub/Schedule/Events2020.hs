{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Data.TheGapChessClub.Schedule.Events2020(
  events2020
) where

import Data.TheGapChessClub.Schedule.TgccEvent(TgccEvent, confirmedTgccEvent, tentativeTgccEvent)
import Data.String
import Data.Int
import Data.Semigroup
import Data.Text.Lazy(Text)
import qualified Data.Text.Lazy as Text(pack)
import Prelude(Num)

events2020 ::
  [TgccEvent]
events2020 =
  [
    lightningRound1
  , lightningRound2
  , prizeNight
  , stDavidsDay
  , floodCupRound1
  , floodCupRound2
  , floodCupRound3
  , floodCupRound4
  , floodCupRound5
  , floodCupRound6
  , floodCupRound7
  , floodCupRoundPlayOff
  , stuartWilsonRound1
  , stuartWilsonRound2
  , stuartWilsonRound3
  , stuartWilsonRound4
  , stuartWilsonRound5
  , stuartWilsonRound6
  , stuartWilsonRound7
  , stuartWilsonRound8
  , stuartWilsonRound9
  , stuartWilsonRoundPlayOff
  , marcusPorterRound1to4
  , marcusPorterRound5to8
  , allegroRoundCommencement
  , allegroRound01
  , allegroRound02
  , allegroRound03
  , allegroRound04
  , allegroRound05
  , allegroRoundFinal
  , swissRound01
  , swissRound02
  , swissRound03
  , swissRound04
  , swissRound05
  ]


lightningRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
lightningRound round month date =
  confirmedTgccEvent
    ("Lightning Tournament round " <> Text.pack round)
    "time control 300+0, 2x round-robin"
    2020
    month
    date
    ["Lightning"]
    
lightningRound1 ::
  TgccEvent
lightningRound1 =
  lightningRound "1" 1 31
    
lightningRound2 ::
  TgccEvent
lightningRound2 =
  lightningRound "2" 2 7
    
prizeNight ::
  TgccEvent
prizeNight =
  confirmedTgccEvent
    "2020 Prize Night and handicap mini-tournament"
    "time control 300+0"
    2020
    2
    21
    ["Prize Night"]
    
stDavidsDay ::
  TgccEvent
stDavidsDay =
  confirmedTgccEvent
    "St David's Day Tournament"
    "time control 300+2, handicap round robin"
    2020
    2
    28
    ["St David's Day"]
    
floodCupRound ::
  Num a =>
  (Text -> String -> a-> b -> c -> [String] -> d)
  -> String
  -> b
  -> c
  -> d
floodCupRound eventStatus round month date =
  eventStatus
    ("Flood Cup round " <> Text.pack round)
    "time control 4200+30, round-robin"
    2020
    month
    date
    ["Flood Cup"]
    
confirmedFloodCupRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
confirmedFloodCupRound =
  floodCupRound confirmedTgccEvent
    
tentativeFloodCupRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
tentativeFloodCupRound =
  floodCupRound tentativeTgccEvent
    
floodCupRound1 ::
  TgccEvent
floodCupRound1 =
  confirmedFloodCupRound "1" 3 6
    
floodCupRound2 ::
  TgccEvent
floodCupRound2 =
  confirmedFloodCupRound "2" 3 13

floodCupRound3 ::
  TgccEvent
floodCupRound3 =
  confirmedFloodCupRound "3" 3 27

floodCupRound4 ::
  TgccEvent
floodCupRound4 =
  confirmedFloodCupRound "4" 4 3

floodCupRound5 ::
  TgccEvent
floodCupRound5 =
  confirmedFloodCupRound "5" 4 24

floodCupRound6 ::
  TgccEvent
floodCupRound6 =
  confirmedFloodCupRound "6" 5 1

floodCupRound7 ::
  TgccEvent
floodCupRound7 =
  confirmedFloodCupRound "7" 5 15

floodCupRoundPlayOff ::
  TgccEvent
floodCupRoundPlayOff =
  tentativeFloodCupRound "play-off" 5 22

stuartWilsonRound ::
  Num a =>
  (Text -> String -> a-> b -> c -> [String] -> d)
  -> String
  -> b
  -> c
  -> d
stuartWilsonRound eventStatus round month date =
  eventStatus
    ("Stuart Wilson Maiden Tournament " <> Text.pack round)
    "time control 3600+10, round-robin"
    2020
    month
    date
    ["Maiden", "Stuart Wilson"]
    
confirmedStuartWilsonRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
confirmedStuartWilsonRound =
  stuartWilsonRound confirmedTgccEvent
    
tentativeStuartWilsonRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
tentativeStuartWilsonRound =
  stuartWilsonRound tentativeTgccEvent

stuartWilsonRound1 ::
  TgccEvent
stuartWilsonRound1 =
  confirmedStuartWilsonRound "1" 5 29

stuartWilsonRound2 ::
  TgccEvent
stuartWilsonRound2 =
  confirmedStuartWilsonRound "2" 6 5

stuartWilsonRound3 ::
  TgccEvent
stuartWilsonRound3 =
  confirmedStuartWilsonRound "3" 6 19

stuartWilsonRound4 ::
  TgccEvent
stuartWilsonRound4 =
  confirmedStuartWilsonRound "4" 6 26

stuartWilsonRound5 ::
  TgccEvent
stuartWilsonRound5 =
  confirmedStuartWilsonRound "5" 7 10

stuartWilsonRound6 ::
  TgccEvent
stuartWilsonRound6 =
  confirmedStuartWilsonRound "6" 7 17

stuartWilsonRound7 ::
  TgccEvent
stuartWilsonRound7 =
  confirmedStuartWilsonRound "7" 7 31

stuartWilsonRound8 ::
  TgccEvent
stuartWilsonRound8 =
  confirmedStuartWilsonRound "8" 8 7

stuartWilsonRound9 ::
  TgccEvent
stuartWilsonRound9 =
  confirmedStuartWilsonRound "9" 8 21

stuartWilsonRoundPlayOff ::
  TgccEvent
stuartWilsonRoundPlayOff =
  tentativeStuartWilsonRound "play-off" 8 28

marcusPorterRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
marcusPorterRound round month date =
  tentativeTgccEvent
    ("Marcus Porter Memorial Tournament " <> Text.pack round)
    "time control 600+2, 8 rounds"
    2020
    month
    date
    ["Marcus Porter"]

marcusPorterRound1to4 ::
  TgccEvent
marcusPorterRound1to4 =
  marcusPorterRound "1 to 4" 10 23

marcusPorterRound5to8 ::
  TgccEvent
marcusPorterRound5to8 =
  marcusPorterRound "5 to 8" 10 30

allegroRound ::
  Num a =>
  (Text -> String -> a-> b -> c -> [String] -> d)
  -> String
  -> b
  -> c
  -> d
allegroRound eventStatus round month date =
  eventStatus
    ("Allegro Tournament " <> Text.pack round)
    "time control 600+10, round-robin"
    2020
    month
    date
    ["Allegro"]
    
confirmedAllegroRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
confirmedAllegroRound =
  allegroRound confirmedTgccEvent
        
allegroRoundCommencement ::
  TgccEvent
allegroRoundCommencement =
  confirmedAllegroRound "commencement" 9 4

allegroRound01 ::
  TgccEvent
allegroRound01 =
  confirmedAllegroRound "allegro in-progress" 9 11

allegroRound02 ::
  TgccEvent
allegroRound02 =
  confirmedAllegroRound "allegro in-progress" 9 18

allegroRound03 ::
  TgccEvent
allegroRound03 =
  confirmedAllegroRound "allegro in-progress" 9 25

allegroRound04 ::
  TgccEvent
allegroRound04 =
  confirmedAllegroRound "allegro in-progress" 10 2

allegroRound05 ::
  TgccEvent
allegroRound05 =
  confirmedAllegroRound "allegro in-progress" 10 9

allegroRoundFinal ::
  TgccEvent
allegroRoundFinal =
  confirmedAllegroRound "allegro final" 10 16

swissRound ::
  Num a =>
  (Text -> String -> a-> b -> c -> [String] -> d)
  -> String
  -> b
  -> c
  -> d
swissRound eventStatus round month date =
  eventStatus
    ("Swiss Tournament " <> Text.pack round)
    "time control 600+10, round-robin"
    2020
    month
    date
    ["Swiss"]
    
confirmedSwissRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
confirmedSwissRound =
  swissRound confirmedTgccEvent
    
swissRound01 ::
  TgccEvent
swissRound01 =
  confirmedSwissRound "1" 11 6

swissRound02 ::
  TgccEvent
swissRound02 =
  confirmedSwissRound "2" 11 13

swissRound03 ::
  TgccEvent
swissRound03 =
  confirmedSwissRound "3" 11 20

swissRound04 ::
  TgccEvent
swissRound04 =
  confirmedSwissRound "4" 11 27

swissRound05 ::
  TgccEvent
swissRound05 =
  confirmedSwissRound "5" 12 4

{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Data.TheGapChessClub.Schedule.Events2019 where

import Data.TheGapChessClub.Schedule.TgccEvent(TgccEvent, confirmedTgccEvent, tentativeTgccEvent)
import Data.String
import Data.Int
import Data.Semigroup
import Data.Text.Lazy(Text)
import qualified Data.Text.Lazy as Text(pack)
import Prelude(Num)

events2019 ::
  [TgccEvent]
events2019 =
  [
    lightningRound1
  , lightningRound2
  , prizeNight
  , stDavidsDay
  , floodCupRound1
  , floodCupRound2
  , floodCupRound3
  , floodCupRound4
  , floodCupRound5
  , floodCupRound6
  , floodCupRound7
  , _50thAnniversayBlitz
  , stuartWilsonRound1
  , stuartWilsonRound2
  , stuartWilsonRound3
  , stuartWilsonRound4
  , stuartWilsonRound5
  , stuartWilsonRound6
  , stuartWilsonRound7
  , stuartWilsonRound8
  , stuartWilsonRound9
  , stuartWilsonRoundPlayOff
  , marcusPorterRound1to4
  , marcusPorterRound5to8
  , allegroRoundCommencement
  , allegroRoundFinal
  , allegroRoundPlayOff
  ]

lightningRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
lightningRound round month date =
  confirmedTgccEvent
    ("Lightning Tournament round " <> Text.pack round)
    "time control 300+0, 2x round-robin"
    2019
    month
    date
    ["Lightning"]
    
lightningRound1 ::
  TgccEvent
lightningRound1 =
  lightningRound "1" 2 1
    
lightningRound2 ::
  TgccEvent
lightningRound2 =
  lightningRound "2" 2 8
    
prizeNight ::
  TgccEvent
prizeNight =
  confirmedTgccEvent
    "2018 Prize Night and handicap mini-tournament"
    "time control 300+0"
    2019
    2
    22
    ["Prize Night"]
    
stDavidsDay ::
  TgccEvent
stDavidsDay =
  confirmedTgccEvent
    "St David's Day Tournament"
    "time control 300+2, handicap round robin"
    2019
    3
    1
    ["St David's Day"]
    
floodCupRound ::
  Num a =>
  (Text -> String -> a-> b -> c -> [String] -> d)
  -> String
  -> b
  -> c
  -> d
floodCupRound eventStatus round month date =
  eventStatus
    ("Flood Cup round " <> Text.pack round)
    "time control 4200+30, round-robin"
    2019
    month
    date
    ["Flood Cup"]
    
confirmedFloodCupRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
confirmedFloodCupRound =
  floodCupRound confirmedTgccEvent
    
tentativeFloodCupRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
tentativeFloodCupRound =
  floodCupRound tentativeTgccEvent
    
floodCupRound1 ::
  TgccEvent
floodCupRound1 =
  confirmedFloodCupRound "1" 3 8
    
floodCupRound2 ::
  TgccEvent
floodCupRound2 =
  confirmedFloodCupRound "2" 3 15

floodCupRound3 ::
  TgccEvent
floodCupRound3 =
  confirmedFloodCupRound "3" 3 29

floodCupRound4 ::
  TgccEvent
floodCupRound4 =
  confirmedFloodCupRound "4" 4 12

floodCupRound5 ::
  TgccEvent
floodCupRound5 =
  confirmedFloodCupRound "5" 4 26

floodCupRound6 ::
  TgccEvent
floodCupRound6 =
  confirmedFloodCupRound "6" 5 3

floodCupRound7 ::
  TgccEvent
floodCupRound7 =
  confirmedFloodCupRound "7" 5 24

floodCupRoundPlayOff ::
  TgccEvent
floodCupRoundPlayOff =
  tentativeFloodCupRound "play-off" 5 31

_50thAnniversayBlitz ::
  TgccEvent
_50thAnniversayBlitz =
  confirmedTgccEvent
    "50th anniversary Blitz Tournament "
    "time control 300+2, round-robin"
    2019
    5
    31
    ["Blitz"]

stuartWilsonRound ::
  Num a =>
  (Text -> String -> a-> b -> c -> [String] -> d)
  -> String
  -> b
  -> c
  -> d
stuartWilsonRound eventStatus round month date =
  eventStatus
    ("Stuart Wilson Maiden Tournament " <> Text.pack round)
    "time control 3600+10, round-robin"
    2019
    month
    date
    ["Maiden", "Stuart Wilson"]
    
confirmedStuartWilsonRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
confirmedStuartWilsonRound =
  stuartWilsonRound confirmedTgccEvent
    
tentativeStuartWilsonRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
tentativeStuartWilsonRound =
  stuartWilsonRound tentativeTgccEvent

stuartWilsonRound1 ::
  TgccEvent
stuartWilsonRound1 =
  confirmedStuartWilsonRound "1" 6 7

stuartWilsonRound2 ::
  TgccEvent
stuartWilsonRound2 =
  confirmedStuartWilsonRound "2" 6 14

stuartWilsonRound3 ::
  TgccEvent
stuartWilsonRound3 =
  confirmedStuartWilsonRound "3" 6 21

stuartWilsonRound4 ::
  TgccEvent
stuartWilsonRound4 =
  confirmedStuartWilsonRound "4" 6 28

stuartWilsonRound5 ::
  TgccEvent
stuartWilsonRound5 =
  confirmedStuartWilsonRound "5" 7 12

stuartWilsonRound6 ::
  TgccEvent
stuartWilsonRound6 =
  confirmedStuartWilsonRound "6" 7 19

stuartWilsonRound7 ::
  TgccEvent
stuartWilsonRound7 =
  confirmedStuartWilsonRound "7" 8 2

stuartWilsonRound8 ::
  TgccEvent
stuartWilsonRound8 =
  confirmedStuartWilsonRound "8" 8 9

stuartWilsonRound9 ::
  TgccEvent
stuartWilsonRound9 =
  confirmedStuartWilsonRound "9" 8 23

stuartWilsonRoundPlayOff ::
  TgccEvent
stuartWilsonRoundPlayOff =
  tentativeStuartWilsonRound "play-off" 8 30

marcusPorterRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
marcusPorterRound round month date =
  tentativeTgccEvent
    ("Marcus Porter Memorial Tournament " <> Text.pack round)
    "time control 600+2, 8 rounds"
    2019
    month
    date
    ["Marcus Porter"]

marcusPorterRound1to4 ::
  TgccEvent
marcusPorterRound1to4 =
  marcusPorterRound "1 to 4" 10 25

marcusPorterRound5to8 ::
  TgccEvent
marcusPorterRound5to8 =
  marcusPorterRound "5 to 8" 11 01

allegroRound ::
  Num a =>
  (Text -> String -> a-> b -> c -> [String] -> d)
  -> String
  -> b
  -> c
  -> d
allegroRound eventStatus round month date =
  eventStatus
    ("Allegro Tournament " <> Text.pack round)
    "time control 600+10, round-robin"
    2019
    month
    date
    ["Allegro"]
    
confirmedAllegroRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
confirmedAllegroRound =
  allegroRound confirmedTgccEvent
    
tentativeAllegroRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
tentativeAllegroRound =
  allegroRound tentativeTgccEvent
    
allegroRoundCommencement ::
  TgccEvent
allegroRoundCommencement =
  confirmedAllegroRound "commencement" 9 6

allegroRoundFinal ::
  TgccEvent
allegroRoundFinal =
  confirmedAllegroRound "final" 11 22

allegroRoundPlayOff ::
  TgccEvent
allegroRoundPlayOff =
  tentativeAllegroRound "play-off" 11 29

{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Data.TheGapChessClub.Schedule.Events(
  events
) where

import Data.Semigroup((<>))
import Data.TheGapChessClub.Schedule.TgccEvent(TgccEvent)
import Data.TheGapChessClub.Schedule.Events2019(events2019)
import Data.TheGapChessClub.Schedule.Events2020(events2020)
import Data.TheGapChessClub.Schedule.Events2021(events2021)

events ::
  [TgccEvent]
events =
  events2019 <> events2020 <> events2021
{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Data.TheGapChessClub.Schedule.Calendar(
  writecalfile
, tgccCalendar
) where

import qualified Data.ByteString.Lazy as ByteString(writeFile)
import Data.Default(def)
import Data.Time(UTCTime, getCurrentTime)
import Data.Version(makeVersion)
import qualified Data.Map as Map(empty, fromList)
import qualified Data.Set as Set(empty, fromList)
import Prelude(FilePath, IO, Maybe(Just), fmap)
import Text.ICalendar(VCalendar(VCalendar), ProdId(ProdId), ICalVersion(MaxICalVersion), Scale(Scale), Method(Method), OtherProperty(OtherProperty), printICalendar)
import Data.TheGapChessClub.Schedule.TgccEvent(TgccEvent, tgccEvent)

writecalfile ::
  FilePath
  -> [TgccEvent]
  -> IO ()
writecalfile file events =
  do  now <- getCurrentTime
      ByteString.writeFile file (printICalendar def (tgccCalendar now events))

tgccCalendar ::
  UTCTime
  -> [TgccEvent]
  -> VCalendar
tgccCalendar now events =
  VCalendar
    (
      ProdId
        "-//The Gap Chess Club//thegapchessclub-schedule//EN"
        def
    )
    (
      MaxICalVersion
        (
          makeVersion [2, 0]
        )
        def
    )
    (
      Scale
        "GREGORIAN"
        def
    )
    (
      Just
        (
          Method
            "PUBLISH"
            def
        )
    )
    (
      Set.fromList
        [
          OtherProperty
            "X-WR-CALNAME"
            "The Gap Chess Club schedule"
            def
        , OtherProperty
            "X-WR-TIMEZONE"
            "Australia/Brisbane"
            def
        , OtherProperty
            "X-WR-CALDESC"
            "Schedule for The Gap Chess Club"
            def
        ]
    )
    Map.empty
    (
      Map.fromList
        (
          fmap (\e -> tgccEvent e now) events
        )
    )
    Map.empty
    Map.empty
    Map.empty
    Set.empty
    
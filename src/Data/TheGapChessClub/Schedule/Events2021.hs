{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Data.TheGapChessClub.Schedule.Events2021(
  events2021
) where

import Data.TheGapChessClub.Schedule.TgccEvent(TgccEvent, confirmedTgccEvent, tentativeTgccEvent)
import Data.String
import Data.Int
import Data.List
import Data.Semigroup
import Data.Text.Lazy(Text, pack)
import qualified Data.Text.Lazy as Text(pack)
import GHC.Show(show)
import Prelude(Num, Integer, (-))

events2021 ::
  [TgccEvent]
events2021 =
  [
    lightningRound1
  , lightningRound2
  , prizeNight
  , stDavidsDay
  , floodCupRound1
  , floodCupRound2
  , floodCupRound3
  , floodCupRound4
  , floodCupRound5
  , floodCupRound6
  , floodCupRound7
  , floodCupRoundPlayOff
  , stuartWilsonRound1
  , stuartWilsonRound2
  , stuartWilsonRound3
  , stuartWilsonRound4
  , stuartWilsonRound5
  , stuartWilsonRound6
  , stuartWilsonRound7
  , stuartWilsonRoundPlayOff
  , marcusPorterRound1to4
  , marcusPorterRound5to8
  , allegroRounds123
  , allegroRounds456
  , allegroRounds789
  , allegroRounds101112
  , allegroRoundPlayOff
  , martinLaizansCup01
  , martinLaizansCup02
  , martinLaizansCup03
  , martinLaizansCup04
  , martinLaizansCup05
  ]

year ::
  Num a =>
  a
year =
  2021

lightningRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
lightningRound round month date =
  confirmedTgccEvent
    ("Lightning Tournament round " <> Text.pack round)
    "time control 300+0 (-handicap), 2x round-robin"
    year
    month
    date
    ["Lightning"]

lightningRound1 ::
  TgccEvent
lightningRound1 =
  lightningRound "1" 2 5

lightningRound2 ::
  TgccEvent
lightningRound2 =
  lightningRound "2" 2 12

prizeNight ::
  TgccEvent
prizeNight =
  confirmedTgccEvent
    (pack (show ((year :: Integer) - 1)) <> " Prize Night and handicap mini-tournament")
    "time control 300+0 (-handicap)"
    year
    2
    26
    ["Prize Night"]

stDavidsDay ::
  TgccEvent
stDavidsDay =
  confirmedTgccEvent
    "St David's Day Tournament"
    "time control 300+3 (Bronstein), handicap round robin"
    year
    3
    5
    ["St David's Day"]

floodCupRound ::
  Num a =>
  (Text -> String -> a-> b -> c -> [String] -> d)
  -> String
  -> b
  -> c
  -> d
floodCupRound eventStatus round month date =
  eventStatus
    ("Flood Cup round " <> Text.pack round)
    "time control 4200+30, round-robin"
    year
    month
    date
    ["Flood Cup"]

confirmedFloodCupRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
confirmedFloodCupRound =
  floodCupRound confirmedTgccEvent

tentativeFloodCupRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
tentativeFloodCupRound =
  floodCupRound tentativeTgccEvent

floodCupRound1 ::
  TgccEvent
floodCupRound1 =
  confirmedFloodCupRound "1" 3 12

floodCupRound2 ::
  TgccEvent
floodCupRound2 =
  confirmedFloodCupRound "2" 3 19

floodCupRound3 ::
  TgccEvent
floodCupRound3 =
  confirmedFloodCupRound "3" 3 26

floodCupRound4 ::
  TgccEvent
floodCupRound4 =
  confirmedFloodCupRound "4" 4 9

floodCupRound5 ::
  TgccEvent
floodCupRound5 =
  confirmedFloodCupRound "5" 4 23

floodCupRound6 ::
  TgccEvent
floodCupRound6 =
  confirmedFloodCupRound "6" 5 7

floodCupRound7 ::
  TgccEvent
floodCupRound7 =
  confirmedFloodCupRound "7" 5 21

floodCupRoundPlayOff ::
  TgccEvent
floodCupRoundPlayOff =
  tentativeFloodCupRound "play-off" 5 28

stuartWilsonRound ::
  Num a =>
  (Text -> String -> a-> b -> c -> [String] -> d)
  -> String
  -> b
  -> c
  -> d
stuartWilsonRound eventStatus round month date =
  eventStatus
    ("Stuart Wilson Maiden Tournament " <> Text.pack round)
    "time control 3600+10, round-robin"
    year
    month
    date
    ["Maiden", "Stuart Wilson"]

confirmedStuartWilsonRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
confirmedStuartWilsonRound =
  stuartWilsonRound confirmedTgccEvent

tentativeStuartWilsonRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
tentativeStuartWilsonRound =
  stuartWilsonRound tentativeTgccEvent

stuartWilsonRound1 ::
  TgccEvent
stuartWilsonRound1 =
  confirmedStuartWilsonRound "1" 6 4

stuartWilsonRound2 ::
  TgccEvent
stuartWilsonRound2 =
  confirmedStuartWilsonRound "2" 6 11

stuartWilsonRound3 ::
  TgccEvent
stuartWilsonRound3 =
  confirmedStuartWilsonRound "3" 6 18

stuartWilsonRound4 ::
  TgccEvent
stuartWilsonRound4 =
  confirmedStuartWilsonRound "4" 7 2

stuartWilsonRound5 ::
  TgccEvent
stuartWilsonRound5 =
  confirmedStuartWilsonRound "5" 7 16

stuartWilsonRound6 ::
  TgccEvent
stuartWilsonRound6 =
  confirmedStuartWilsonRound "6" 7 30

stuartWilsonRound7 ::
  TgccEvent
stuartWilsonRound7 =
  confirmedStuartWilsonRound "7" 8 13

stuartWilsonRoundPlayOff ::
  TgccEvent
stuartWilsonRoundPlayOff =
  tentativeStuartWilsonRound "play-off" 8 20

allegroRound ::
  Num a =>
  (Text -> String -> a -> b -> c -> [String] -> d)
  -> [String]
  -> b
  -> c
  -> d
allegroRound eventStatus rounds month date =
  eventStatus
    ("Allegro Tournament " <> Text.pack (intercalate ", " rounds))
    "time control 600+10, round-robin"
    year
    month
    date
    ["Allegro"]

confirmedAllegroRound ::
  [String]
  -> Int
  -> Int
  -> TgccEvent
confirmedAllegroRound =
  allegroRound confirmedTgccEvent

allegroRounds123 ::
  TgccEvent
allegroRounds123 =
  confirmedAllegroRound ["1", "2", "3"] 9 17

allegroRounds456 ::
  TgccEvent
allegroRounds456 =
  confirmedAllegroRound ["4", "5", "6"] 9 24

allegroRounds789 ::
  TgccEvent
allegroRounds789 =
  confirmedAllegroRound ["7", "8", "9"] 10 1

allegroRounds101112 ::
  TgccEvent
allegroRounds101112 =
  confirmedAllegroRound ["10", "11", "12"] 10 8

allegroRoundPlayOff ::
  TgccEvent
allegroRoundPlayOff =
  confirmedAllegroRound ["Play-off"] 10 15

marcusPorterRound ::
  String
  -> Int
  -> Int
  -> TgccEvent
marcusPorterRound round month date =
  tentativeTgccEvent
    ("Marcus Porter Memorial Tournament " <> Text.pack round)
    "time control 600+2, 8 rounds"
    year
    month
    date
    ["Marcus Porter"]

marcusPorterRound1to4 ::
  TgccEvent
marcusPorterRound1to4 =
  marcusPorterRound "1 to 4" 10 22

marcusPorterRound5to8 ::
  TgccEvent
marcusPorterRound5to8 =
  marcusPorterRound "5 to 8" 10 29

martinLaizansCup ::
  Num a =>
  (Text -> String -> a-> b -> c -> [String] -> d)
  -> String
  -> b
  -> c
  -> d
martinLaizansCup eventStatus round month date =
  eventStatus
    ("Martin Laizans Cup " <> Text.pack round)
    "time control 3600+10, round-robin"
    year
    month
    date
    ["Swiss"]

confirmedMartinLaizansCup ::
  String
  -> Int
  -> Int
  -> TgccEvent
confirmedMartinLaizansCup =
  martinLaizansCup confirmedTgccEvent

martinLaizansCup01 ::
  TgccEvent
martinLaizansCup01 =
  confirmedMartinLaizansCup "1" 11 5

martinLaizansCup02 ::
  TgccEvent
martinLaizansCup02 =
  confirmedMartinLaizansCup "2" 11 12

martinLaizansCup03 ::
  TgccEvent
martinLaizansCup03 =
  confirmedMartinLaizansCup "3" 11 19

martinLaizansCup04 ::
  TgccEvent
martinLaizansCup04 =
  confirmedMartinLaizansCup "4" 11 26

martinLaizansCup05 ::
  TgccEvent
martinLaizansCup05 =
  confirmedMartinLaizansCup "5" 12 3
